import axios from "axios";
import { EventBus } from './event-bus.js';

import { all } from "q";
export default {
  data() {
    return {
      value: 0,
      dialog: false,
      titulo: "algo",
      search: "",
      rowsPerPageItems: [2, 8, 12, 50],
      pagination: {
        rowsPerPage: 2
      },
      textsnackbar: "My timeout is set to 2000.",
      timeout: 2000,
      data_grafico: [123, 89, 10],
      itemsdata: [
        {
          planta: "Planta sulfuro",
          ok: 237,
          ejecucion: 20,
          retrasada: 1,
          responsable: "agapito",
          planta: "planta tratamiento",
          ok: 237,
          ejecucion: 20,
          retrasada: 1,
          responsable: "agapito",
          planta: "fundicion silicato",
          ok: 237,
          ejecucion: 20,
          retrasada: 5,
          responsable: "agapito",
          data_grafico: [123, 89, 10]
        },
        {
          planta: "Linea b-1",
          ok: 237,
          ejecucion: 20,
          retrasada: 3,
          responsable: "agapito",
          planta: "linea b-2",
          ok: 237,
          ejecucion: 20,
          retrasada: 2,
          responsable: "agapito",
          data_grafico: [456, 189, 10]
        },
        {
          planta: "planta tratamiento",
          ok: 237,
          ejecucion: 20,
          retrasada: 1,
          responsable: "agapito",
          data_grafico: [789, 389, 10]
        }
      ],
      verListaTarjetas: true,
      aqui: false,

      seriesAzul: [],
      chartOptionsAzul: {
        chart: {
          height: 350,
          type: "bar"
        },
        plotOptions: {
          bar: {
            dataLabels: {
              position: "top" // top, center, bottom
            }
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val + " ";
          },
          offsetY: -10,
          style: {
            fontSize: "12px",
            colors: ["#304758"]
          }
        },

        xaxis: {
          categories: ["ok", "ejecutada", "retrasada"],
          position: "bottom",
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: "gradient",
              gradient: {
                colorFrom: "#D8E3F0",
                colorTo: "#BED1E6",
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5
              }
            }
          },
          tooltip: {
            enabled: true
          }
        },
        yaxis: {
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          labels: {
            show: false,
            formatter: function (val) {
              return val + "%";
            }
          }
        },
        title: {
          text: "Monthly Inflation in Argentina, 2002",
          floating: true,
          offsetY: 330,
          align: "center",
          style: {
            color: "#444"
          }
        }
      },
      wcmAzul: [],
      Usuarios: [],
      UsuariosAreas: [],
      UsuarioSingle: [],
      Avatar: "",
      NombreUsuario: "",
      // MATRIZ DATA
      dataok: [],
      DataModificadaOK: [],
      datajecucion: [],
      DataModificadaEJECUCION: [],
      dataretrasada: [],
      DataModificadaRETRASADA: [],
      DataModificadaTABLAMain: [],
      idusuario: "",
      idusuarioKey: 0
    };
  },
  props: {
    //idusuario: {
    // type: Number,
    // default: 0
    //}
  },
  watch: {
    loader() {
      const l = this.loader;
      this[l] = !this[l];

      setTimeout(() => (this[l] = false), 3000);

      this.loader = null;
    }
  },
  mounted() {
    EventBus.$on('emittedResponsableDetalle', data => {
      console.log("recibiendo dato detalle id usuario ", data.id)
      console.log("recibiendo dato para  ", data.ver)
      this.value = data;
      this.dialog = data.ver;
      this.idusuario = data.id;
      if (this.idusuario != this.idusuarioKey) {
        this.DataModificadaTABLAMain= [];
        this.idusuarioKey = this.idusuarioKey;
        this.wcmAzulData();
        this.listarUsuarios();
        this.listarUsuariosAreas();
        this.GetAvatar();
        this.GetdataOk();
        this.GetdataEjecucion();
        this.GetdataRetrasado();
      }

    });
    //EventBus.$off('emittedResponsableDetalle');

  },
  created() {

    // console.log("DATOS FINALES",THIS.)
  },
  computed: {

    CurrentAvatar: function () {
      return this.Avatar;
    },
    FormateoDataTabla: function () {
      let me = this;
      if (me.DataModificadaOK.length > 0) {
        console.log("entro1");
        if (me.DataModificadaEJECUCION.length > 0) {
          console.log("entro2");
          if (me.DataModificadaRETRASADA.length > 0) {
            console.log("entro3");
            this.FormateoMesesDataUnionOER();
            //this.loading = false;
          }
        }
      }
    }
  },
  methods: {
    FormateoRetrasadaData() {
      let me = this;
      var i = 0;
      me.UsuariosAreas.forEach(function (element) {
        //console.log(array[i].id)
        var item = me.dataretrasada.find(x => x.idarea === element.idarea);
        if (item) {
          // console.log("MES:", element.mes, "Total:", item.total);
          me.DataModificadaRETRASADA.push({
            idarea: element.idarea,
            nombre: item.nombre,
            anio: item.anio,
            totalretrasada: item.total
          });
        } else {
          // console.log("MES sin dato", element.mes, "Total:", 0);
          me.DataModificadaRETRASADA.push({
            idarea: element.idarea,
            nombre: element.area,
            anio: 0,
            totalretrasada: 0
          });
        }

        i++;
        //console.log(i);
      });
    },
    FormateoEjecucionData() {
      let me = this;
      var i = 0;
      me.UsuariosAreas.forEach(function (element) {
        //console.log(array[i].id)
        var item = me.datajecucion.find(x => x.idarea === element.idarea);
        if (item) {
          // console.log("MES:", element.mes, "Total:", item.total);
          me.DataModificadaEJECUCION.push({
            idarea: element.idarea,
            nombre: item.nombre,
            anio: item.anio,
            totalejecucion: item.total
          });
        } else {
          // console.log("MES sin dato", element.mes, "Total:", 0);
          me.DataModificadaEJECUCION.push({
            idarea: element.idarea,
            nombre: element.area,
            anio: 0,
            totalejecucion: 0
          });
        }

        i++;
        //console.log(i);
      });
    },
    FormateoOkData() {
      let me = this;
      var i = 0;
      me.UsuariosAreas.forEach(function (element) {
        //console.log(array[i].id)
        var item = me.dataok.find(x => x.idarea === element.idarea);
        if (item) {
          // console.log("MES:", element.mes, "Total:", item.total);
          me.DataModificadaOK.push({
            idarea: element.idarea,
            nombre: item.nombre,
            anio: item.anio,
            totalok: item.total
          });
        } else {
          // console.log("MES sin dato", element.mes, "Total:", 0);
          me.DataModificadaOK.push({
            idarea: element.idarea,
            nombre: element.area,
            anio: 0,
            totalok: 0
          });
        }

        i++;
        //console.log(i);
      });
      console.log("DATOS FINALES", me.DataModificadaTABLAMain);
    },
    listarUsuarios() {
      let me = this;
      axios
        .get("api/RegistrosanomaliasMatriz/MatrizDatosResponsable")
        .then(function (response) {
          me.Usuarios = response.data;
          //   console.log("Usuarios", me.Usuarios);
        })
        .catch(function (error) {
          console.log(error);
          //catch the error, check it has a response object with lodash
          if ((error, "response")) {
            console.log("entro a los errroes");
            //console.log(error);
            if (error.message === "Network Error") {
              //me.errorMsg("El servidor se encuentra suspendido...")
              me.msgNotificacion("Información", "Servidor suspendido", "error");
              /*server is down*/
            }

            if (error.response.status == 500) {
              me.msgNotificacion(
                "Información",
                "Ocurrio un error de conexión a Internet o la red...",
                "error"
              );
              // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
            }
            //console.log(error.response.data);
          } else {
            console.log(
              "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
            );
            console.log("error response property is undefined");
          }
        });
    },
    listarUsuariosAreas() {
      let me = this;
      axios
        .get("api/RegistrosanomaliasMatriz/MatrizDatosAreas/" + this.idusuario)
        .then(function (response) {
          me.UsuariosAreas = response.data;
          // console.log("Usuarios", me.UsuariosAreas);
        })
        .catch(function (error) {
          console.log(error);
          //catch the error, check it has a response object with lodash
          if ((error, "response")) {
            console.log("entro a los errroes");
            //console.log(error);
            if (error.message === "Network Error") {
              //me.errorMsg("El servidor se encuentra suspendido...")
              me.msgNotificacion("Información", "Servidor suspendido", "error");
              /*server is down*/
            }

            if (error.response.status == 500) {
              me.msgNotificacion(
                "Información",
                "Ocurrio un error de conexión a Internet o la red...",
                "error"
              );
              // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
            }
            //console.log(error.response.data);
          } else {
            console.log(
              "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
            );
            console.log("error response property is undefined");
          }
        });
    },
    GetAvatar() {
      let me = this;
      axios
        .get("api/RegistrosanomaliasMatriz/GetUsuarioResponsable/" + this.idusuario)
        .then(function (response) {
          me.Avatar = response.data[0].avatar;
          me.NombreUsuario = response.data[0].nombre;
          //console.log("FOTO OPERADOR", response.data[0].avatar);
        })
        .catch(function (error) {
          console.log(error);
          //catch the error, check it has a response object with lodash
          if ((error, "response")) {
            console.log("entro a los errroes");
            //console.log(error);
            if (error.message === "Network Error") {
              //me.errorMsg("El servidor se encuentra suspendido...")
              me.msgNotificacion("Información", "Servidor suspendido", "error");
              /*server is down*/
            }

            if (error.response.status == 500) {
              me.msgNotificacion(
                "Información",
                "Ocurrio un error de conexión a Internet o la red...",
                "error"
              );
              // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
            }
            //console.log(error.response.data);
          } else {
            console.log(
              "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
            );
            console.log("error response property is undefined");
          }
        });
    },
    GetdataOk() {
      let me = this;
      axios
        .get("/api/RegistrosanomaliasMatriz/MatrizDatos/2020/OK/" + this.idusuario)
        .then(function (response) {
          me.dataok = response.data;
          console.log("DATOS OK TARJetas", response.data);
          me.FormateoOkData();
        })
        .catch(function (error) {
          console.log(error);
          //catch the error, check it has a response object with lodash
          if ((error, "response")) {
            console.log("entro a los errroes");
            //console.log(error);
            if (error.message === "Network Error") {
              //me.errorMsg("El servidor se encuentra suspendido...")
              me.msgNotificacion("Información", "Servidor suspendido", "error");
              /*server is down*/
            }

            if (error.response.status == 500) {
              me.msgNotificacion(
                "Información",
                "Ocurrio un error de conexión a Internet o la red...",
                "error"
              );
              // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
            }
            //console.log(error.response.data);
          } else {
            console.log(
              "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
            );
            console.log("error response property is undefined");
          }
        });
    },
    GetdataEjecucion() {
      let me = this;
      axios
        .get("/api/RegistrosanomaliasMatriz/MatrizDatos/2020/EJECUCION/" + this.idusuario)
        .then(function (response) {
          me.datajecucion = response.data;
          console.log("DATOS EJECUCION TARJetas", response.data);
          me.FormateoEjecucionData();
          console.log("DATOS EJECUCION MODIFICADA", me.DataModificadaEJECUCION);
        })
        .catch(function (error) {
          console.log(error);
          //catch the error, check it has a response object with lodash
          if ((error, "response")) {
            console.log("entro a los errroes");
            //console.log(error);
            if (error.message === "Network Error") {
              //me.errorMsg("El servidor se encuentra suspendido...")
              me.msgNotificacion("Información", "Servidor suspendido", "error");
              /*server is down*/
            }

            if (error.response.status == 500) {
              me.msgNotificacion(
                "Información",
                "Ocurrio un error de conexión a Internet o la red...",
                "error"
              );
              // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
            }
            //console.log(error.response.data);
          } else {
            console.log(
              "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
            );
            console.log("error response property is undefined");
          }
        });
    },
    GetdataRetrasado() {
      let me = this;
      axios
        .get("/api/RegistrosanomaliasMatriz/MatrizDatos/2020/RETRASADO/" + this.idusuario)
        .then(function (response) {
          me.dataretrasada = response.data;
          console.log("DATOS RETRASADO TARJetas", response.data);
          me.FormateoRetrasadaData();
          console.log("DATOS RETRASADO MODIFICADO", me.DataModificadaRETRASADA);
        })
        .catch(function (error) {
          console.log(error);
          //catch the error, check it has a response object with lodash
          if ((error, "response")) {
            console.log("entro a los errroes");
            //console.log(error);
            if (error.message === "Network Error") {
              //me.errorMsg("El servidor se encuentra suspendido...")
              me.msgNotificacion("Información", "Servidor suspendido", "error");
              /*server is down*/
            }

            if (error.response.status == 500) {
              me.msgNotificacion(
                "Información",
                "Ocurrio un error de conexión a Internet o la red...",
                "error"
              );
              // me.errorMsg(" Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet..")
            }
            //console.log(error.response.data);
          } else {
            console.log(
              "Lo más probable es un tiempo de espera del servidor o un error de conexión a Internet o la red"
            );
            console.log("error response property is undefined");
          }
        });
    },
    FormateoMesesDataUnionOER() {
      let me = this;
      var i = 0;
      me.UsuariosAreas.forEach(function (element) {
        //console.log(array[i].id)
        var itemOk = me.DataModificadaOK.find(x => x.idarea === element.idarea);
        var itemEjecucion = me.DataModificadaEJECUCION.find(
          x => x.idarea === element.idarea
        );
        var itemRetrasado = me.DataModificadaRETRASADA.find(
          x => x.idarea === element.idarea
        );

        // var dataItems =
        //  itemA.totalReport + itemB.totalReport + itemC.totalReport;
        if (itemOk && itemEjecucion && itemRetrasado) {
          me.DataModificadaTABLAMain.push({
            idarea: element.idarea,
            nombre: element.area,
            // anio: element.anio,
            totalok: itemOk.totalok,
            totalejecucion: itemEjecucion.totalejecucion,
            totalretrasada: itemRetrasado.totalretrasada
          });
        } else {
          // console.log("MES sin dato", element.mes, "Total:", 0);
          me.DataModificadaTABLAMain.push({
            idarea: element.idarea,
            nombre: element.area,
            //anio: element.anio,
            totalok: 0,
            totalejecucion: 0,
            totalretrasada: 0
          });
        }

        i++;
        //console.log(i);
      });
    },

    wcmAzulData() {
      this.wcmAzul.push({
        name: "Inflation",
        data: this.data_grafico
      });
      this.seriesAzul = this.wcmAzul;
    },
    tableHeaderColor({ row, column, rowIndex, columnIndex }) {
      //var cant = row.length()
      if (rowIndex === 0 && columnIndex === 0) {
        return "background-color: #1306c5;color: white;font-weight: 500;";
        azul;
      }
      if (rowIndex === 0 && columnIndex === 1) {
        return "background-color: #A3E4D7;color: green;font-weight: 500;";
        verde;
      }
      if (rowIndex === 0 && columnIndex === 2) {
        return "background-color: #F9E79F;color: #EB984E;font-weight: 500;";
        amarillo;
      }
      if (rowIndex === 0 && columnIndex === 3) {
        return "background-color: #FFB6C1;color: red;font-weight: 500;";
        rojo;
      }
      if (columnIndex === 0) {
        return "background-color: #F0F3F4;color: black;font-weight: 500;";
      }
    }
  },
  msgNotificacion(titulo, mensaje, tipo) {
    this.$notify({
      title: titulo,
      message: mensaje,
      type: tipo
    });
  }
};